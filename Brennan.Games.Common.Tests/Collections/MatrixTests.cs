﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Brennan.Games.Common.Collections;
using Brennan.Games.Common.Numeric;

namespace Brennan.Games.Common.Tests.Collections
{
    public class MatrixTests
    {
        [Test]
        public void testConstruction()
        {
            Matrix<int> matrix = new Matrix<int>(new Vector2Int(0, 0));
            Assert.IsNotNull(matrix);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void testConstructionNegativeSizeThrows()
        {
            Matrix<int> matrix = new Matrix<int>(new Vector2Int(0, -1));
        }

        [Test]
        public void testLength()
        {
            Vector2Int expectedSize = new Vector2Int(1, 2);
            Matrix<String> matrix = new Matrix<String>(expectedSize);
            Assert.AreEqual(expectedSize, matrix.Length);
        }

        [Test]
        public void testIndexerSetAndGet()
        {
            String value = "Test Value";
            Vector2Int index = new Vector2Int(1, 1);
            Matrix<String> matrix = new Matrix<String>(new Vector2Int(5, 5));
            Assert.IsNull(matrix[index]);
            matrix[index] = value;
            Assert.AreEqual(value, matrix[index]);
        }

        [Test]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public void testIndexerGetThrowsIfOutsideRange()
        {
            Vector2Int index = new Vector2Int(6, 1);
            Matrix<String> matrix = new Matrix<String>(new Vector2Int(5, 5));
            String value = matrix[index];
        }

        [Test]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public void testIndexerGetThrowsIfNegative()
        {
            Vector2Int index = new Vector2Int(4, -1);
            Matrix<String> matrix = new Matrix<String>(new Vector2Int(5, 5));
            String value = matrix[index];
        }

        [Test]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public void testIndexerSetThrowsIfOutsideRange()
        {
            Vector2Int index = new Vector2Int(6, 1);
            Matrix<String> matrix = new Matrix<String>(new Vector2Int(5, 5));
            matrix[index] = "Some value";
        }

        [Test]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public void testIndexerSetThrowsIfNegative()
        {
            Vector2Int index = new Vector2Int(4, -1);
            Matrix<String> matrix = new Matrix<String>(new Vector2Int(5, 5));
            matrix[index] = "Some Value";
        }

        [Test]
        public void testGetEnumerator()
        {
            int count = 0;
            Vector2Int size = new Vector2Int(5, 5);
            Matrix<String> matrix = new Matrix<String>(size);
            foreach (String s in matrix)
            {
                count++;
            }
            Assert.AreEqual(size.X * size.Y, count);
        }
    }
}
