﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Brennan.Games.Common;
using Brennan.Games.Common.Numeric;

namespace CollisionSample
{
    public partial class Form1 : Form
    {
        Bitmap Backbuffer;

        PhysicsEngine engine;

        Random random;

        int fixedFramesPerSec = 50;

        public Form1()
        {
            InitializeComponent();

            random = new Random(2);

            this.SetStyle(
            ControlStyles.UserPaint |
            ControlStyles.AllPaintingInWmPaint |
            ControlStyles.DoubleBuffer, true);

            Timer DrawTimer = new Timer();
            DrawTimer.Interval = 30;
            DrawTimer.Tick += new EventHandler(DrawTimer_Tick);

            Timer PhysicsTimer = new Timer();
            PhysicsTimer.Interval = 20;
            PhysicsTimer.Tick += new EventHandler(PhysicsTimer_Tick);

            this.ResizeEnd += new EventHandler(Form1_CreateBackBuffer);
            this.Load += new EventHandler(Form1_CreateBackBuffer);
            this.Paint += new PaintEventHandler(Form1_Paint);

            this.ClientSize = new Size(1000, 1000);

            engine = new PhysicsEngine(new Vector2Int(ClientSize.Width, ClientSize.Height));

            PhysicsObjectView gov;
            int radious = 5;

            for (int x = 50; x < ClientSize.Width - 50; x = x + radious * 2)
            {
                gov = engine.CreateGameObjectView(new Vector2Int(x, ClientSize.Height), radious, ColliderEnum.Circle, random.Next(1, 50));
                gov.Velocity = new Vector2Int(0, -100);

                gov = engine.CreateGameObjectView(new Vector2Int(x + radious, 0), radious, ColliderEnum.Circle, random.Next(1, 50));
                gov.Velocity = new Vector2Int(0, 200);
            }

            for (int y = 50; y < ClientSize.Height - 50; y = y + radious * 2)
            {
                gov = engine.CreateGameObjectView(new Vector2Int(ClientSize.Width, y), radious, ColliderEnum.Circle, random.Next(1, 50));
                gov.Velocity = new Vector2Int(-200, 0);

                gov = engine.CreateGameObjectView(new Vector2Int(0, y + radious), radious, ColliderEnum.Rectangle, random.Next(1, 50));
                gov.Velocity = new Vector2Int(300, 0);
            }

            PhysicsObjectView inmovableObject = engine.CreateGameObjectView(new Vector2Int(ClientSize.Width / 2, ClientSize.Height / 2), radious, ColliderEnum.Rectangle, 0);


            //gov = engine.CreateGameObjectView(new Vector2Int(100, 100), radious, ColliderEnum.Circle, 1);
            //gov = engine.CreateGameObjectView(new Vector2Int(100, 100), radious, ColliderEnum.Circle, 1);

            //GameObjectView a = engine.CreateGameObject(new Vector2Int(100, 100), 15, ColliderEnum.Circle);
            //a.Velocity = new Vector2Int(50, 0);

            //GameObjectView b = engine.CreateGameObject(new Vector2Int(200, 120), 15, ColliderEnum.Rectangle);
            //b.Velocity = new Vector2Int(-50, 0);

            PhysicsTimer.Start();
            DrawTimer.Start();
        }


        void Form1_Paint(object sender, PaintEventArgs e)
        {
            if (Backbuffer != null)
            {
                e.Graphics.DrawImageUnscaled(Backbuffer, Point.Empty);
            }
        }

        void Form1_CreateBackBuffer(object sender, EventArgs e)
        {
            if (Backbuffer != null)
                Backbuffer.Dispose();

            Backbuffer = new Bitmap(ClientSize.Width, ClientSize.Height);
        }

        void Draw()
        {
            if (Backbuffer != null)
            {
                using (var g = Graphics.FromImage(Backbuffer))
                {
                    g.Clear(Color.White);

                    //Draw Grid Lines
                    for (int i = 0; i < ClientSize.Height; i = i + 10)
                    {
                        g.DrawLine(Pens.BlanchedAlmond, 0, ClientSize.Height - i, ClientSize.Width, ClientSize.Height - i);
                    }

                    for (int i = 0; i < ClientSize.Width; i = i + 10)
                    {
                        g.DrawLine(Pens.BlanchedAlmond, i, 0, i, ClientSize.Height);
                    }

                    Circle c;
                    Brennan.Games.Common.Rectangle r;
                    foreach (PhysicsObjectView gameObject in engine.PhysicsObjectViews)
                    {
                        switch (gameObject.ColliderType)
                        {
                            case ColliderEnum.Circle:
                                c = gameObject.Collider as Circle;
                                g.FillEllipse(Brushes.DarkBlue, c.Center.X - c.Radious, ClientSize.Height - c.Center.Y - c.Radious, c.Radious * 2, c.Radious * 2);
                                break;
                            case ColliderEnum.Rectangle:
                                r = gameObject.Collider as Brennan.Games.Common.Rectangle;
                                g.FillRectangle(Brushes.Black, r.Min.X, ClientSize.Height - r.Max.Y, r.Size.X, r.Size.Y);
                                break;
                        }
                        
                    }
                }

                Invalidate();
            }
        }

        void PhysicsTimer_Tick(object sender, EventArgs e)
        {
            engine.Update(fixedFramesPerSec);
        }

        
        void DrawTimer_Tick(object sender, EventArgs e)
        {
            Draw();
        }

        private Color GetRandomColor()
        {
            return Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
            // The error is here
        }  
    }
}
