﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

using Brennan.Games.PathFinding.TwoDAStar;
using Brennan.Games.Common;
using Brennan.Games.Common.Numeric;
using Brennan.Games.PathFinding;
using Brennan.Games.PathFinding.Vector2IntPathFinder;

namespace RTSPhysicsSample
{
    public partial class PathfindingSample : Form
    {
        public const int gridWidth = 512;
        public const int gridHeight = 512;
        public const int cellSize = 1;

        IEnumerable<Vector2Int> path;
        ShortestPathFinder<Vector2Int> shortestPathFinder;
        int screenWidth;
        int screenHeihgt;

        List<Vector2Int> occupiedSpace;

        public PathfindingSample()
        {
            InitializeComponent();

            //this.Paint += new PaintEventHandler(RTSPhysicsSample_Paint);

            screenWidth = gridWidth * cellSize;
            screenHeihgt = gridHeight * cellSize;

            ClientSize = new Size(screenWidth, screenHeihgt);

            SetStyle(ControlStyles.ResizeRedraw, true);

            //shortestPathFinder = new AStarGrid(gridWidth, gridHeight);
            shortestPathFinder = new Vector2IntAStar(new Vector2Int(gridWidth, gridHeight));

            occupiedSpace = new List<Vector2Int>();

            Vector2Int point;
            for (ushort x = 10; x < gridWidth; x++)
            {
                point = new Vector2Int(x, gridHeight - 10);
                occupiedSpace.Add(point);
                shortestPathFinder.OccupySpace(point);
            }

            for (ushort y = 10; y < gridHeight - 10; y++)
            {
                point = new Vector2Int(10, y);
                shortestPathFinder.OccupySpace(point);
                occupiedSpace.Add(point);
            }
        }

        void Panel_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            int x;
            int y;
            foreach (Vector2Int p in occupiedSpace)
            {
                x = p.X * cellSize;
                y = screenHeihgt - p.Y * cellSize; //convert y to screen coordinates 
                g.FillRectangle(Brushes.Black, x, y, cellSize, cellSize);
            }

            foreach (Vector2Int p in path)
            {
                x = p.X * cellSize;
                y = screenHeihgt - p.Y * cellSize; //convert y to screen coordinates 
                g.FillRectangle(Brushes.Blue, x, y, cellSize, cellSize);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Stopwatch sp = new Stopwatch();

            sp.Start();
            path = shortestPathFinder.FindShortestPath(new Vector2Int(gridWidth / 2, gridHeight / 2), new Vector2Int(gridWidth - 1, gridHeight - 1));
            sp.Stop();

            this.label1.Text = "Search time: " + sp.ElapsedMilliseconds;

            this.panel1.Paint += Panel_Paint;

            this.panel1.Invalidate();
        }
        
    }
}
