﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Brennan.Games.PathFinding.FlowField;
using Brennan.Games.Common.Numeric;

namespace Brennan.Games.PathFinding.Tests.FlowField
{
    public class IntegratorTests
    {
        private static int WIDTH = 10;
        private static int HEIGHT = 10;

        [Test]
        public void testIntegrate()
        {
            CostField costField = new CostField(new ClearCostField(WIDTH, HEIGHT));
            Integrator integrator = new Integrator(costField);
            IntegrationField integratedField = integrator.Integrate(new Vector2Int(0, 0));
            for (int i = 0; i < HEIGHT; i++)
            {
                for (int j = 0; j < WIDTH; j++)
                {
                    System.Console.Write(integratedField.IntegratedField[j, i].IntegratedCost + " ");
                }
                System.Console.WriteLine();
            }

            System.Console.WriteLine();

            foreach (KeyValuePair<Vector2Int, int> pair in integrator.numTimesIntegrated)
            {
                System.Console.WriteLine(pair.Key + " : " + pair.Value);
            }

            Assert.IsNotNull(integratedField);
        }

        [Test]
        public void testIntegrate2()
        {
            CostField costField = new CostField(new ClearCostField(WIDTH, HEIGHT));
            costField.Costs[new Vector2Int(0, 3)] = 2;
            costField.Costs[new Vector2Int(1, 3)] = 4;
            costField.Costs[new Vector2Int(2, 3)] = 10;
            costField.Costs[new Vector2Int(3, 3)] = 60;
            costField.Costs[new Vector2Int(3, 4)] = 60;
            costField.Costs[new Vector2Int(3, 5)] = 60;
            costField.Costs[new Vector2Int(3, 6)] = 60;
            costField.Costs[new Vector2Int(2, 6)] = 2;
            Integrator integrator = new Integrator(costField);
            IntegrationField integratedField = integrator.Integrate(new Vector2Int(5, 5));
            for (int i = 0; i < HEIGHT; i++)
            {
                for (int j = 0; j < WIDTH; j++)
                {
                    System.Console.Write(integratedField.IntegratedField[j, i].IntegratedCost + " ");
                }
                System.Console.WriteLine();
            }

            System.Console.WriteLine();

            foreach (KeyValuePair<Vector2Int, int> pair in integrator.numTimesIntegrated)
            {
                System.Console.WriteLine(pair.Key + " : " + pair.Value);
            }

            Assert.IsNotNull(integratedField);
        }
    }
}
