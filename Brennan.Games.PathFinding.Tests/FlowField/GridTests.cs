﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.PathFinding.FlowField;
using Brennan.Games.Common.Numeric;
using NUnit.Framework;

namespace Brennan.Games.PathFinding.Tests.FlowField
{
    class GridTests
    {
        public static int WIDTH = 100;
        public static int HEIGHT = 100;
        public static int SECTOR_SIZE = 10;

        public Grid grid;

        [SetUp]
        public void setup()
        {
            grid = new Grid(WIDTH, HEIGHT, SECTOR_SIZE);
        }

        #region GetSectorFromSectorCOrdinate
        [Test]
        public void testGetSectorFromSectorCordinateReturnsSector()
        {
            Sector sector = grid.GetSectorFromSectorCordinate(new Vector2Int(1, 1));
            Assert.IsNotNull(sector);
        }

        [Test]
        public void testGetSectorFromSectorCordinateOutsideRangeReturnsNull()
        {
            Sector sector = grid.GetSectorFromSectorCordinate(grid.SectorDimension);
            Assert.IsNull(sector);
        }

        [Test]
        public void testGetSectorFromSectorCordinateWithNegativeCordinateReturnsNull()
        {
            Sector sector = grid.GetSectorFromSectorCordinate(new Vector2Int(-1, -1));
            Assert.IsNull(sector);
        }
        #endregion

        #region GetSectorFromCellGridCordinate
        [Test]
        public void testGetSectorFromCellGridCordinateReturnsSector()
        {
            Sector sector = grid.GetSectorFromCellGridCordinate(new Vector2Int(50, 50));
            Assert.IsNotNull(sector);
        }

        [Test]
        public void testGetSectorFromCellGridCordinateOutsideRangeReturnsNull()
        {
            Sector sector = grid.GetSectorFromCellGridCordinate(grid.SectorDimension * grid.SectorSize);
            Assert.IsNull(sector);
        }
        #endregion

        #region SetCellCost

        [Test]
        public void testSetCellCost()
        {
            Vector2Int cellGridCordinate = new Vector2Int(0,0);
            Vector2Int cellSectorCordinate = grid.GetSectorCellCordinate(cellGridCordinate);
            byte newCost = 1;
            grid.SetCellCost(cellGridCordinate, newCost);
            Sector sector = grid.GetSectorFromCellGridCordinate(cellGridCordinate);
            byte actualCost = sector.CostField.GetCost(cellSectorCordinate);
            Assert.AreEqual(newCost, actualCost);

            cellGridCordinate = new Vector2Int(98, 76);
            cellSectorCordinate = grid.GetSectorCellCordinate(cellGridCordinate);
            grid.SetCellCost(cellGridCordinate, newCost);
            sector = grid.GetSectorFromCellGridCordinate(cellGridCordinate);
            actualCost = sector.CostField.GetCost(cellSectorCordinate);
            Assert.AreEqual(newCost, actualCost);
        }

        #endregion
    }
}
