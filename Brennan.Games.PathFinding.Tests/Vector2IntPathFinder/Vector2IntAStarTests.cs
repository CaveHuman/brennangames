﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Brennan.Games.PathFinding.Vector2IntPathFinder;
using Brennan.Games.Common.Numeric;
using Brennan.Games.PathFinding.TwoDAStar;

namespace Brennan.Games.PathFinding.Tests.Vector2IntPathFinder
{
    class Vector2IntAStarTests
    {
        [Test]
        public void test()
        {
            int p = 2;
            Vector2Int size = new Vector2Int(10, 10) * p;
            ShortestPathFinder<Vector2Int> astar = new Vector2IntAStar(size);
            //ShortestPathFinder<Vector2Int> astar = new AStarGrid(size.X, size.Y);
            Vector2Int start = Vector2Int.Zero;
            Vector2Int goal = size - Vector2Int.One - (Vector2Int.Right * (2 * p));
            IEnumerable<Vector2Int> path = astar.FindShortestPath(start, goal);
            foreach (Vector2Int point in path)
            {
                System.Console.WriteLine(point);
            }
        }
    }
}
