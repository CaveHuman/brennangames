﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.PathFinding.AStar;
using Brennan.Games.Common.Numeric;

namespace Brennan.Games.PathFinding.Vector2IntPathFinder
{
    public class Vector2IntHeuristicManager : HeuristicManager<Vector2Int>
    {
        private List<Vector2Int> goals = new List<Vector2Int>();

        public Vector2IntHeuristicManager(Vector2Int goal)
        {
            goals.Add(goal);
        }

        public Vector2IntHeuristicManager(List<Vector2Int> goals)
        {
            foreach (Vector2Int vect in goals)
            {
                goals.Add(vect);
            }
        }

        private int EstimateCost(Vector2Int a, Vector2Int b)
        {
            int xDiff = Math.Abs(a.X - b.X);
            int yDiff = Math.Abs(a.Y - b.Y);

            //estimated distance is move as far diagonal as possible to goal, then move in a horizontal or vertical line
            return timesDiagonal((Math.Min(xDiff, yDiff)) + Math.Abs(xDiff - yDiff));
        }

        public int EstimateCost(Vector2Int a)
        {
            return EstimateCost(a, GetGoal(a));
        }

        public bool ReachedGoal(Vector2Int a)
        {
            foreach (Vector2Int goal in goals)
            {
                if (goal.Equals(a))
                {
                    return true;
                }
            }
            return false;
        }


        public Vector2Int GetGoal(Vector2Int a)
        {
            return goals[0];
        }

        public static int timesDiagonal(int val) 
        {
            return (val * 7) / 5; // 7/5 = 1.4 = sqrt of 2 rounded
        }
    }
}
