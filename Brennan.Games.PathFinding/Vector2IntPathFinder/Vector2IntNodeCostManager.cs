﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.Common.Numeric;

namespace Brennan.Games.PathFinding.Vector2IntPathFinder
{
    public interface Vector2IntNodeCostManager
    {
        byte getCost(Vector2Int point);
    }
}
