﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.Common.Numeric;
using Brennan.Games.Common.Collections;

namespace Brennan.Games.PathFinding.Vector2IntPathFinder
{
    public class Vector2IntNodeManager
    {
        private Vector2IntNodeCostManager costManager;
        private Matrix<Vector2IntNode> nodesInfo;

        public Vector2IntNodeManager(Vector2IntNodeCostManager costManager, Vector2Int size)
        {
            this.costManager = costManager;
            nodesInfo = new Matrix<Vector2IntNode>(size);
            setupNodesInfo();
        }

        private void setupNodesInfo()
        {
            bool isWalkable = true;
            foreach (Vector2Int point in nodesInfo.Length)
            {
                nodesInfo[point] = new Vector2IntNode(this, point, costManager.getCost(point), isWalkable);
            }
        }

        public Vector2IntNode this[Vector2Int index]
        {
            get
            {
                return nodesInfo[index];
            }
        }

        public bool Contains(Vector2Int point)
        {
            return nodesInfo.Contains(point);
        }

        public Vector2IntNode getNode(Vector2Int vector2Int)
        {
            return this[vector2Int];
        }
    }
}
