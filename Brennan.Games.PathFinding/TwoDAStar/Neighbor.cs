﻿using System;

namespace Brennan.Games.PathFinding.TwoDAStar
{
    internal class Neighbor
    {
        internal GridNode Point { get; set; }
        internal int Cost { get; set; }
        internal Direction DirectionFromParent { get; set; }

        internal Neighbor(GridNode point, int cost, Direction direction)
        {
            Point = point;
            Cost = cost;
            DirectionFromParent = direction;
        }
    }
}
