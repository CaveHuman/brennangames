﻿using System;
using Sys;

using Brennan.Games.Common.Collections.Heaps;

namespace Brennan.Games.PathFinding.TwoDAStar
{
    public class GridNode : HasIndex, IComparable
    {

        public int X;
        public int Y;

        private int hash;

        public int Index { get; set; }
        public bool InClosedSet { get; set; }
        public bool InOpenSet { get; set; }
        public int GScore { get; set; }
        public int FScore { get; set; }
        internal Direction DirectionFromParent { get; set; }

        public GridNode parent { get; set; }

        public bool Walkable { get; set; }

        public GridNode(int x, int y)
        {
            X = x;
            Y = y;

            hash = Tuple.Create(X, Y).GetHashCode();

            InClosedSet = false;
            InOpenSet = false;
            Walkable = true;
        }

        public override bool Equals(Object obj)
        {
            if (!(obj is GridNode)) return false;

            GridNode p = (GridNode)obj;
            return X == p.X & Y == p.Y;
        }

        public override int GetHashCode()
        {
            return hash;
        }

        public int CompareTo(object obj)
        {
            return FScore.CompareTo(((GridNode)obj).FScore);
        }
    }
}
