﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Brennan.Games.PathFinding.AStar
{
    class NodesPathData<T> : IComparer<Node<T>>
    {
        private Dictionary<Node<T>, NodePathData<T>> nodesPathData;

        public NodesPathData()
        {
            nodesPathData = new Dictionary<Node<T>, NodePathData<T>>();
        }

        public NodePathData<T> this[Node<T> node]
        {
            get
            {
                if (!nodesPathData.ContainsKey(node))
                {
                    nodesPathData[node] = new NodePathData<T>();
                }
                return nodesPathData[node];
            }
        }

        public int Compare(Node<T> x, Node<T> y)
        {
            return nodesPathData[x].CompareTo(nodesPathData[y]);
        }
    }
}
