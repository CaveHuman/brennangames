﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Brennan.Games.PathFinding.AStar
{
    public interface HeuristicManager <T>
    {
        int EstimateCost(T a);

        bool ReachedGoal(T a);

        T GetGoal(T a);
    }
}
