﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.Common.Numeric;

namespace Brennan.Games.PathFinding.FlowField
{
    public class ClearCostField : CostField
    {
        public ClearCostField(int width, int height)
            : base(width, height)
        {
            foreach (Vector2Int point in new Vector2Int(width, height))
            {
                Costs[point] = CLEAR;
            }
        }
    }
}
