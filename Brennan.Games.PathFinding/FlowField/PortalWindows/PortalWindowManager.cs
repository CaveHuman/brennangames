﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.Common.Collections;
using Brennan.Games.Common.Numeric;
using Brennan.Games.PathFinding.FlowField;
using Brennan.Games.PathFinding.FlowField.Edges;

namespace Brennan.Games.PathFinding.FlowField.PortalWindows
{
    public class PortalWindowManager
    {
        private Grid grid;
        private SectorEdges sectorEdges;

        public PortalWindowManager(Grid grid)
        {
            this.grid = grid;
            this.sectorEdges = grid.SectorEdges;
            InitPortalWindows();
        }

        private void InitPortalWindows()
        {
            foreach (Vector2Int sectorCordinate in grid.SectorDimension)
            {
                MakeTopAndLeftPortalWindowsForSector(sectorCordinate);
            }
        }

        /// <summary>
        /// Makes only the Top and Left portal windows so when looping through all sectors
        /// each edge is only calculated once
        /// </summary>
        /// <param name="sectorCordinate"></param>
        private void MakeTopAndLeftPortalWindowsForSector(Vector2Int sectorCordinate)
        {
            Sector sector = grid.GetSectorFromSectorCordinate(sectorCordinate);
            if (sector != null)
            {
                Sector left = grid.GetSectorFromSectorCordinate(sectorCordinate + Vector2Int.Left);
                if (left != null)
                {
                    MakePortalWindowsForAdjacentSectors(left, sector, sectorEdges.RightEdge, sectorEdges.LeftEdge);
                }
                Sector top = grid.GetSectorFromSectorCordinate(sectorCordinate + Vector2Int.Up);
                if (top != null)
                {
                    MakePortalWindowsForAdjacentSectors(top, sector, sectorEdges.BottomEdge, sectorEdges.TopEdge);
                }
            }
        }

        private void MakePortalWindowsForAdjacentSectors(Sector a, Sector b, SectorEdge edgeA, SectorEdge edgeB)
        {
            Vector2Int startA = Vector2Int.Zero;
            Vector2Int startB = Vector2Int.Zero;
            Vector2Int endA = Vector2Int.Zero;
            Vector2Int endB = Vector2Int.Zero;
            bool foundEmpty = false;

            foreach (ZipEntry<Vector2Int, Vector2Int> zipEntry in EnumerableExtensions.Zip<Vector2Int, Vector2Int>(edgeA, edgeB))
            {
                if (a.CostField.Costs[endA] != CostField.IMPASSABLE &&
                    b.CostField.Costs[endB] != CostField.IMPASSABLE)
                {
                    if (!foundEmpty)
                    {
                        startA = zipEntry.Value1;
                        startB = zipEntry.Value2;
                        foundEmpty = true;
                    }
                }
                else
                {
                    if (foundEmpty)
                    {
                        MakePortalPair(a, b, startA, endA, startB, endB, edgeA, edgeB);
                        foundEmpty = false;
                    }
                }
                endA = zipEntry.Value1;
                endB = zipEntry.Value2;
            }

            if (foundEmpty)
            {
                MakePortalPair(a, b, startA, endA, startB, endB, edgeA, edgeB);
            }
        }

        private void MakePortalPair(Sector a, Sector b, Vector2Int aStart, Vector2Int aEnd, Vector2Int bStart, Vector2Int bEnd, SectorEdge aEdge, SectorEdge bEdge)
        {
            PortalWindow aWindow = new PortalWindow(a, aStart, aEnd);
            PortalWindow bWindow = new PortalWindow(b, bStart, bEnd);
            aWindow.PortalPair = new PortalLink(bWindow, 1);
            bWindow.PortalPair = new PortalLink(aWindow, 1);
            a.Windows.Add(aWindow, aEdge);
            b.Windows.Add(bWindow, bEdge);
        }

        public void UpdateSectorPortalWindows(Sector sector, Vector2Int updatedCellSectorCordinate)
        {
            if (sector.CostField.GetCost(updatedCellSectorCordinate) != CostField.IMPASSABLE)
            {
                return;
            }
            List<PortalWindow> windowList;
            List<PortalWindow> removedWindows = new List<PortalWindow>();
            foreach (SectorEdge sectorEdge in sectorEdges)
            {
                windowList = sector.Windows.GetPortalWindowList(sectorEdge);
                foreach (PortalWindow pw in windowList)
                {
                    if (updatedCellSectorCordinate.WithinRangeInclusive(pw.Min, pw.Max))
                    {
                        //check if it's at the beggining, end or middle of the list
                        if (updatedCellSectorCordinate == pw.Min)
                        {
                            pw.RemoveMin();
                            pw.PortalPair.Node.RemoveMin();
                            if (pw.Empty)
                            {
                                removedWindows.Add(pw);
                            }
                        }
                        else if (updatedCellSectorCordinate == pw.Max)
                        {
                            pw.RemoveMax();
                            pw.PortalPair.Node.RemoveMax();
                            if (pw.Empty)
                            {
                                removedWindows.Add(pw);
                            }
                        }
                        else
                        {
                            SplitPortalWindow(sector, pw, sectorEdge, updatedCellSectorCordinate);
                            removedWindows.Add(pw);
                        }
                    }
                }
                ClearRemovedPortalWindows(removedWindows, sector, sectorEdge);
                removedWindows.Clear();
            }
        }

        private void SplitPortalWindow(Sector sector, PortalWindow pw, SectorEdge edge, Vector2Int splitCell)
        {
            Vector2Int aEnd, bStart;
            if(edge.Axes == EdgeAxes.Horizontal)
            {
                aEnd = splitCell + Vector2Int.Left;
                bStart = splitCell + Vector2Int.Right;
            }
            else 
            {
                aEnd = splitCell + Vector2Int.Down;
                bStart = splitCell + Vector2Int.Up;
            }
            MakePortalPair(sector, pw.PortalPair.Node.Sector, pw.Min, aEnd, bStart, pw.Max, edge, edge.Opposite);
        }

        private void ClearRemovedPortalWindows(List<PortalWindow> removedPortalWindows, Sector sector, SectorEdge sectorEdge)
        {
            foreach (PortalWindow pw in removedPortalWindows)
            {
                sector.Windows.RemovePortalPair(sectorEdge, pw);
            }
        }
    }
}
