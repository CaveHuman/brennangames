﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.Common.Numeric;

namespace Brennan.Games.PathFinding.FlowField.Edges
{
    public class TopSectorEdge : SectorEdgeBase
    {
        override public EdgeAxes Axes { get { return EdgeAxes.Horizontal; } }

        public TopSectorEdge(Vector2Int sectorSize) : base(sectorSize) { }

        override public bool ContainsCell(Common.Numeric.Vector2Int cellSectorCordinate)
        {
            return cellSectorCordinate.Y == sectorSize.Y - 1;
        }

        override public IEnumerator<Vector2Int> GetEnumerator()
        {
            for (int x = 0; x < sectorSize.X; x++)
            {
                yield return new Vector2Int(x, sectorSize.Y - 1);
            }
        }
    }
}
