﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.Common.Numeric;

namespace Brennan.Games.PathFinding.FlowField.Edges
{
    public interface SectorEdge : IEnumerable <Vector2Int>
    {
        bool ContainsCell(Vector2Int cellSectorCordinate);

        EdgeAxes Axes { get; }

        SectorEdge Opposite { get; set; }
    }
}
