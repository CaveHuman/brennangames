﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.Common.Numeric;
using Brennan.Games.Common.Collections;

namespace Brennan.Games.PathFinding.FlowField
{
    [Serializable()]
    public class CostField
    {
        public static byte IMPASSABLE = byte.MaxValue;
        public static byte CLEAR = 1;

        public Matrix<byte> Costs { get; private set; }
        public int Width { get; private set; }
        public int Height { get; private set; }

        public CostField(int width, int height)
        {
            this.Width = width;
            this.Height = height;
            Costs = new Matrix<byte>(new Vector2Int(width, height));
        }

        public CostField(CostField costField)
        {
            Costs = (Matrix<byte>)costField.Costs.Clone();
            this.Width = Costs.Length.X;
            this.Height = Costs.Length.Y;
        }

        public void SetCost(Vector2Int cellSectorCordinate, byte newValue)
        {
            Costs[cellSectorCordinate] = newValue;
        }

        public byte GetCost(Vector2Int cellSectorCordinate)
        {
            return Costs[cellSectorCordinate];
        }
    }
}
