﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.Common.Numeric;
using Brennan.Games.Common.Collections;
using Brennan.Games.Common.Collections.Heaps;

namespace Brennan.Games.PathFinding.FlowField
{
    public class Integrator
    {
        public int Width;
        public int Height;
        private CostField costField;
        private IntegrationField integrationField;// = new IntegrationField(Width, Height);
        public Dictionary<Vector2Int, int> numTimesIntegrated;

        public Integrator(CostField costField)
        {
            this.costField = costField;
            this.Width = costField.Width;
            this.Height = costField.Height;
            numTimesIntegrated = new Dictionary<Vector2Int, int>();
        }

        public IntegrationField Integrate(Vector2Int initialWaveFront)
        {
            integrationField = new IntegrationField(Width, Height);
            integrationField.IntegratedField[initialWaveFront.X, initialWaveFront.Y].IntegratedCost = 0;
            integrationField.IntegratedField[initialWaveFront.X, initialWaveFront.Y].Integrated = true;

            PrioritySet<IntegrationCell> activeWaveFront = new PrioritySet<IntegrationCell>();
            activeWaveFront.Push(integrationField.IntegratedField[initialWaveFront.X, initialWaveFront.Y]);

            IntegrationCell currentIntegrationCell;
            IntegrationCell neighborIntegrationCell;
            Vector2Int currentSectorCellCordinate;
            int currentIntegratedCost;
            byte currentCost;
            byte neighborCost;
            int newIntegratedCost;
            while(activeWaveFront.Count > 0)
            {
                currentIntegrationCell = activeWaveFront.Pop();
                currentSectorCellCordinate = currentIntegrationCell.SectorCellCordinate;
                updateNumTimesIntegrated(currentSectorCellCordinate);
                currentCost = costField.Costs[currentSectorCellCordinate];
                currentIntegratedCost = currentIntegrationCell.IntegratedCost;
                foreach (Vector2Int neighbor in GetNeighborsThatArePassable(currentSectorCellCordinate))
                {
                    neighborIntegrationCell = integrationField.IntegratedField[neighbor.X, neighbor.Y];
                    if (!neighborIntegrationCell.Integrated)
                    {
                        neighborCost = costField.Costs[neighbor];
                        newIntegratedCost = neighborCost + currentIntegratedCost;
                        if (newIntegratedCost < neighborIntegrationCell.IntegratedCost)
                        {
                            neighborIntegrationCell.IntegratedCost = newIntegratedCost;
                            if (activeWaveFront.Contains(neighborIntegrationCell))
                            {
                                activeWaveFront.Update(neighborIntegrationCell);
                            }
                        }
                        activeWaveFront.Push(neighborIntegrationCell);
                    }
                }
                currentIntegrationCell.Integrated = true;
            }

            return integrationField;
        }

        private void updateNumTimesIntegrated(Vector2Int sectorCellCordinate)
        {
            int oldValue = 0;
            if (numTimesIntegrated.ContainsKey(sectorCellCordinate))
            {
                oldValue = numTimesIntegrated[sectorCellCordinate];
            }
            numTimesIntegrated[sectorCellCordinate] = oldValue + 1;
        }

        private IEnumerable<Vector2Int> GetNeighborsThatArePassable(Vector2Int sectorCell)
        {
            foreach(Vector2Int neighbor in GetNeighbors(sectorCell))
            {
                if (costField.Costs[neighbor] != CostField.IMPASSABLE)
                {
                    yield return neighbor;
                }
            }
        }

        private IEnumerable<Vector2Int> GetNeighbors(Vector2Int sectorCell)
        {
            if(sectorCell.X > 0) yield return sectorCell + Vector2Int.Left;
            if(sectorCell.X < Width - 1) yield return sectorCell + Vector2Int.Right;
            if(sectorCell.Y < Height - 1) yield return sectorCell + Vector2Int.Up;
            if(sectorCell.Y > 0) yield return sectorCell + Vector2Int.Down;
        }
    }
}
