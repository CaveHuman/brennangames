﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Brennan.Games.PathFinding
{
    public interface ShortestPathFinder <T>
    {
        void OccupySpace(T space);
        void UnoccupySpace(T space);
        IEnumerable<T> FindShortestPath(T start, T goal);
    }
}
