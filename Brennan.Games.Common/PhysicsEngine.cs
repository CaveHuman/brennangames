﻿using System;
using System.Collections.Generic;
using Brennan.Games.Common.Collections;
using Brennan.Games.Common.Numeric;
using Brennan.Games.Common.Collections;

namespace Brennan.Games.Common
{
    public class PhysicsEngine
    {
        //private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public const int GameWorldScale = 100;
        internal const int DefaultMass = 1;

        private QuadTree broadPhaseCollisionDetector;

        public List<PhysicsObject> PhysicsObjects;
        public List<PhysicsObjectView> PhysicsObjectViews { get; private set; }

        private int Friction;

        /// <summary>
        /// The world size in game world numbers.
        /// </summary>
        internal Vector2Int worldSize;
        /// <summary>
        /// The world size in real world numbers.
        /// </summary>
        public Vector2Int WorldSize
        {
            get
            {
                return worldSize / GameWorldScale;
            }
        }

        public Vector2Int GameWorldSize
        {
            get
            {
                return worldSize;
            }
        }

        public PhysicsEngine(Vector2Int worldSize)
        {
            init(worldSize, true, 10 * GameWorldScale);
        }

        public PhysicsEngine(Vector2Int size, bool needToConvert)
        {
            init(size, needToConvert, 10 * (needToConvert ? 1 : GameWorldScale));
        }

        public PhysicsEngine(Vector2Int size, bool needToConvert, int xzFriction)
        {
            init(size, needToConvert, xzFriction * (needToConvert ? 1 : GameWorldScale));
        }

        private void init(Vector2Int size, bool needToConvert, int xzFriction)
        {
            if (needToConvert)
            {
                this.worldSize = size * GameWorldScale;
            }
            else
            {
                this.worldSize = size;
            }
            PhysicsObjects = new List<PhysicsObject>();
            PhysicsObjectViews = new List<PhysicsObjectView>();
            Friction = xzFriction;

            broadPhaseCollisionDetector = new QuadTree(5, 10, new Rectangle(worldSize));
        }

        public PhysicsObjectView CreateGameObjectView(Vector2Int initialCenter, int radious, ColliderEnum colliderType)
        {
            return CreateGameObjectView(initialCenter, radious, colliderType, DefaultMass);
        }

        public PhysicsObjectView CreateGameObjectView(Vector2Int initialCenter, int radious, ColliderEnum colliderType, int mass)
        {
            PhysicsObject gameObject = new PhysicsObject(initialCenter * GameWorldScale, radious * GameWorldScale, colliderType, mass);

            AddGameObject(gameObject);

            PhysicsObjectView gameObjectView = new PhysicsObjectView(gameObject);
            PhysicsObjectViews.Add(gameObjectView);

            return gameObjectView;
        }

        public void AddGameObject(PhysicsObject po)
        {
            PhysicsObjects.Add(po);
            broadPhaseCollisionDetector.Insert(po);
        }

        public void Update(int fixedFramesPerSecond)
        {

            OrderedSet<CollisionBroadPhase> broadCollisions = broadPhaseCollisionDetector.GetAllCollisions();

            foreach (CollisionBroadPhase broadCollision in broadCollisions)
            {
                CollisionResolver.ResolveCollision(broadCollision);
            }

            foreach (PhysicsObject physicsObject in PhysicsObjects)
            {
                if (physicsObject.MassGWN > 0 && physicsObject.Velocity != Vector2Int.Zero)
                {
                    //remove from quad tree as it's position is about to change
                    broadPhaseCollisionDetector.Remove(physicsObject);

                    physicsObject.Center = physicsObject.Center + (physicsObject.Velocity / fixedFramesPerSecond);

                    //apply friction
                    physicsObject.Velocity = physicsObject.Velocity.MoveTowardsZero(Friction / fixedFramesPerSecond);

                    broadPhaseCollisionDetector.Insert(physicsObject);
                }
                
            }

            
        }

        #region Collision Methods
        /// <summary>
        /// Checks if the physics objects collides in the narrow phase with
        /// any other object.
        /// This is slower then CollidesBroad, but is more accurate
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool CollidesNarrow(PhysicsObject obj)
        {
            List<PhysicsObject> objects = broadPhaseCollisionDetector.GetCollisions(obj);

            if (objects != null && objects.Count > 0)
            {
                CollisionBroadPhase broadCollision;
                CollisionNarrowPhase narrowCollision;
                foreach (PhysicsObject other in objects)
                {
                    broadCollision = new CollisionBroadPhase(obj, other);
                    narrowCollision = CollisionNarrowPhase.GetNarrowCollision(broadCollision);
                    if (narrowCollision != null)
                    {
                        return true; 
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Checks if the physics objects collides in the narrow phase with
        /// any other object.
        /// This is slower then CollidesBroad, but is more accurate
        /// </summary>
        /// <param name="rect"></param>
        /// <returns></returns>
        public bool CollidesNarrow(Rectangle rect)
        {
            List<PhysicsObject> objects = broadPhaseCollisionDetector.GetCollisions(rect);

            if (objects != null && objects.Count > 0)
            {
                CollisionBroadPhase broadCollision;
                CollisionNarrowPhase narrowCollision;
                PhysicsObject obj = new PhysicsObject(rect, 1/*does not really matter as this is only temp object*/);
                foreach (PhysicsObject other in objects)
                {
                    broadCollision = new CollisionBroadPhase(obj, other);
                    narrowCollision = CollisionNarrowPhase.GetNarrowCollision(broadCollision);
                    if (narrowCollision != null)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Checks if the physics object collides in the broad phase with
        /// any other object.
        /// This is faster then CollidesNarrow, but is less accurate.
        /// (This only checks AABB vs AABB)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool CollidesBroad(PhysicsObject obj)
        {
            List<PhysicsObject> objects = broadPhaseCollisionDetector.GetCollisions(obj);
            if (objects != null && objects.Count > 0)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks if the physics object collides in the broad phase with
        /// any other object.
        /// This is faster then CollidesNarrow, but is less accurate.
        /// (This only checks AABB vs AABB)
        /// </summary>
        /// <param name="rect"></param>
        /// <returns></returns>
        public bool CollidesBroad(Rectangle rect)
        {
            List<PhysicsObject> objects = broadPhaseCollisionDetector.GetCollisions(rect);
            if (objects != null && objects.Count > 0)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Returns all of the Physics Objects that collides with the
        /// rectangle.
        /// </summary>
        /// <param name="rect"></param>
        /// <returns></returns>
        public List<PhysicsObject> GetCollisionsBroad(Rectangle rect)
        {
            return broadPhaseCollisionDetector.GetCollisions(rect);
        }

        /// <summary>
        /// Returns all of the Physics Objects that collides with the
        /// rectangle.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public List<PhysicsObject> GetCollisionsBroad(PhysicsObject obj)
        {
            return broadPhaseCollisionDetector.GetCollisions(obj);
        }

        /// <summary>
        /// Returns all of the physics objects that collides with the
        /// rectangle.
        /// </summary>
        /// <param name="rect"></param>
        /// <returns></returns>
        public List<PhysicsObject> GetCollisionsNarrow(Rectangle rect)
        {
            List<PhysicsObject> objects = broadPhaseCollisionDetector.GetCollisions(rect);
            List<PhysicsObject> narrowObjects = new List<PhysicsObject>(objects.Count);

            if (objects != null && objects.Count > 0)
            {
                CollisionBroadPhase broadCollision;
                CollisionNarrowPhase narrowCollision;
                PhysicsObject obj = new PhysicsObject(rect, 1/*does not really matter as this is only temp object*/);
                foreach (PhysicsObject other in objects)
                {
                    broadCollision = new CollisionBroadPhase(obj, other);
                    narrowCollision = CollisionNarrowPhase.GetNarrowCollision(broadCollision);
                    if (narrowCollision != null)
                    {
                        narrowObjects.Add(other);
                    }
                }
            }

            return narrowObjects;
        }

        /// <summary>
        /// Returns all of the physics objects that collides with the
        /// rectangle.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public List<PhysicsObject> GetCollisionsNarrow(PhysicsObject obj)
        {
            List<PhysicsObject> objects = broadPhaseCollisionDetector.GetCollisions(obj);
            List<PhysicsObject> narrowObjects = new List<PhysicsObject>(objects.Count);

            if (objects != null && objects.Count > 0)
            {
                CollisionBroadPhase broadCollision;
                CollisionNarrowPhase narrowCollision;
                foreach (PhysicsObject other in objects)
                {
                    broadCollision = new CollisionBroadPhase(obj, other);
                    narrowCollision = CollisionNarrowPhase.GetNarrowCollision(broadCollision);
                    if (narrowCollision != null)
                    {
                        narrowObjects.Add(other);
                    }
                }
            }

            return narrowObjects;
        }
        #endregion Collision Methods
    }
}
