﻿using System;
using Sys;

namespace Brennan.Games.Common.Numeric.Unused
{
    public struct Vector2Ratio : IComparable<Vector2Ratio>
    {
        public Ratio X;
        public Ratio Y;

        public Ratio Length
        {
            get
            {
                Ratio temp = LengthSquard;
                return new Ratio(
                    (int) Math.Sqrt(temp.Numerator),
                    (int) Math.Sqrt(temp.Denominator)
                    );
            }
        }

        public Ratio LengthSquard
        {
            get
            {
                return (X * X) + (Y * Y);
            }
        }


        public Vector2Ratio(Ratio x, Ratio y)
        {
            X = x;
            Y = y;
        }

        public Vector2Ratio(int x, int y)
        {
            X = new Ratio(x);
            Y = new Ratio(y);
        }

        #region Vector2Ratio Vector2Ratio Operators
        public static Vector2Ratio operator +(Vector2Ratio A, Vector2Ratio B)
        {
            return new Vector2Ratio(A.X + B.X, A.Y + B.Y);
        }

        public static Vector2Ratio operator -(Vector2Ratio A, Vector2Ratio B)
        {
            return new Vector2Ratio(A.X - B.X, A.Y - B.Y);
        }

        public static Vector2Ratio operator *(Vector2Ratio A, Ratio i)
        {
            return new Vector2Ratio(A.X * i, A.Y * i);
        }

        public static Vector2Ratio operator *(Ratio i, Vector2Ratio A)
        {
            return A * i;
        }

        /// <summary>
        /// Returns the dot product
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <returns></returns>
        public static Ratio operator *(Vector2Ratio A, Vector2Ratio B)
        {
            return (A.X * B.X) + (A.Y * B.Y);
        }

        public static bool operator ==(Vector2Ratio A, Vector2Ratio B)
        {
            if (A.X == B.X && A.Y == B.Y) { return true; }

            return false;
        }

        public static bool operator !=(Vector2Ratio A, Vector2Ratio B)
        {
            return !(A == B);
        }
        #endregion Vector2Ratio Vector2Ratio Operators

        #region Vector2Ratio Vector2Int Operators
        /// <summary>
        /// Returns the dot product
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <returns></returns>
        public static Ratio operator *(Vector2Ratio A, Vector2Int B)
        {
            return (A.X * B.X) + (A.Y * B.Y);
        }

        /// <summary>
        /// Returns the dot product
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <returns></returns>
        public static Ratio operator *(Vector2Int A, Vector2Ratio B)
        {
            return (A.X * B.X) + (A.Y * B.Y);
        }

        /// <summary>
        /// Allows implicit conversion from Vector2Int to Vector2Ratio
        /// </summary>
        /// <param name="vect2Int"></param>
        /// <returns></returns>
        public static implicit operator Vector2Ratio(Vector2Int vect2Int)
        {
            return new Vector2Ratio(vect2Int.X, vect2Int.Y);
        }

        /// <summary>
        /// Allows explicit conversion from Vector2Ratio to Vector2Int
        /// </summary>
        /// <param name="vect2Ratio"></param>
        /// <returns></returns>
        public static explicit operator Vector2Int(Vector2Ratio vect2Ratio)
        {
            return new Vector2Int(vect2Ratio.X.Resolve(), vect2Ratio.Y.Resolve());
        }
        #endregion Vector2Ratio Vector2Int Operators

        #region Vector2Ratio Ratio Operators

        public static Vector2Ratio operator /(Vector2Ratio A, Ratio B)
        {
            return new Vector2Ratio(A.X / B, A.Y / B);
        }

        public static Vector2Ratio operator /(Ratio A, Vector2Ratio B)
        {
            return new Vector2Ratio(A / B.X, A / B.Y);
        }

        #endregion

        /// <summary>
        /// Returns the same Vector2Ratio with its
        /// X and Y components in their most reduced form
        /// </summary>
        /// <returns></returns>
        public Vector2Ratio Reduce()
        {
            return new Vector2Ratio(X.Reduce(), Y.Reduce());
        }

        /// <summary>
        /// Returns the absolute value of this vector.
        /// </summary>
        /// <returns></returns>
        public Vector2Ratio Abs()
        {
            return new Vector2Ratio(
                Ratio.Abs(X),
                Ratio.Abs(Y)
                );
        }

        /// <summary>
        /// Clamps the values in the passed in vector
        /// by the positive and negative x and y of this
        /// vector.
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        public Vector2Ratio Clamp(Vector2Ratio values)
        {
            Vector2Ratio abs = Abs();
            return new Vector2Ratio(
                values.X.Clamp(-1 * abs.X, abs.X),
                values.Y.Clamp(-1 * abs.Y, abs.Y)
                );
        }

        public Vector2Ratio SetX(Ratio x)
        {
            return new Vector2Ratio(x, Y);
        }

        public Vector2Ratio SetY(Ratio y)
        {
            return new Vector2Ratio(X, y);
        }

        public int CompareTo(Vector2Ratio other)
        {
            int res = X.CompareTo(other.X);
            if (res == 0)
            {
                return Y.CompareTo(other.Y);
            }

            return res;
        }

        public override bool Equals(object obj)
        {
            if (obj is Vector2Ratio)
            {
                Vector2Ratio other = (Vector2Ratio)obj;
                if (X == other.X && Y == other.Y)
                {
                    return true;
                }
            }

            return false;
        }

        public override int GetHashCode()
        {
            return (new Tuple<Ratio, Ratio>(X, Y)).GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("({0}, {1})", X, Y);
        }
    }
}
