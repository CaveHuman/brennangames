﻿using System;
using Sys;
using System.Collections.Generic;


namespace Brennan.Games.Common.Numeric
{
    [Serializable]
    public struct Vector2Int : IComparable<Vector2Int>, IEnumerable<Vector2Int>
    {
        public int X;
        public int Y;

        public int Length
        {
            get
            {
                //return (int)Math.Sqrt(LengthSquard);
                return (int)IntExtensions.ISqrt(LengthSquard);
            }
        }

        public int LengthSquard
        {
            get
            {
                return (X * X) + (Y * Y);
            }
        }

        public Vector2Int(int x, int y)
        {
            
            X = x;
            Y = y;
        }

        #region Vector2Int Vector2Int Operators
        public static Vector2Int operator +(Vector2Int A, Vector2Int B)
        {
            return new Vector2Int(A.X + B.X, A.Y + B.Y);
        }

        public static Vector2Int operator -(Vector2Int A, Vector2Int B)
        {
            return new Vector2Int(A.X - B.X, A.Y - B.Y);
        }

        public static Vector2Int operator /(Vector2Int A, Vector2Int B)
        {
            return new Vector2Int(A.X / B.X, A.Y / B.Y);
        }

        /// <summary>
        /// Returns the dot product
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <returns></returns>
        public static int operator *(Vector2Int A, Vector2Int B)
        {
            return (A.X * B.X) + (A.Y * B.Y);
        }

        public static bool operator ==(Vector2Int A, Vector2Int B)
        {
            return (A.X == B.X) && (A.Y == B.Y);
        }

        public static bool operator !=(Vector2Int A, Vector2Int B)
        {
            return (A.X != B.X) || (A.Y != B.Y);
        }
        #endregion Vector2Int Vector2Int Operators

        #region Vector2Int Int Operators

        public static Vector2Int operator +(Vector2Int A, int i)
        {
            return new Vector2Int(A.X + i, A.Y + i);
        }

        public static Vector2Int operator +(int i, Vector2Int A)
        {
            return A + i;
        }

        public static Vector2Int operator -(Vector2Int A, int i)
        {
            return new Vector2Int(A.X - i, A.Y - i);
        }

        public static Vector2Int operator -(int i, Vector2Int A)
        {
            return new Vector2Int(i - A.X, i - A.Y);
        }

        public static Vector2Int operator *(Vector2Int A, int i)
        {
            return new Vector2Int(A.X * i, A.Y * i);
        }

        public static Vector2Int operator *(int i, Vector2Int A)
        {
            return A * i;
        }

        public static Vector2Int operator /(Vector2Int A, int i)
        {
            return new Vector2Int(A.X / i, A.Y / i);
        }

        public static Vector2Int operator /(int i, Vector2Int A)
        {
            return new Vector2Int(i / A.X, i / A.Y);
        }

        public static Vector2Int operator %(Vector2Int A, int i)
        {
            return new Vector2Int(A.X % i, A.Y % i);
        }

        public static Vector2Int operator %(int i, Vector2Int A)
        {
            return new Vector2Int(i % A.X, i % A.Y);
        }
        #endregion Vector2Int Int Operators

        public Vector2Int DivRoundUp(int i)
        {
            return new Vector2Int(
                X % i == 0 ? (X / i) : (X / i) + 1,
                Y % i == 0 ? (Y / i) : (Y / i) + 1
                );
        }

        /// <summary>
        /// Returns the absolute value of this vector.
        /// </summary>
        /// <returns></returns>
        public Vector2Int Abs()
        {
            return new Vector2Int(
                Math.Abs(X),
                Math.Abs(Y)
                );
        }

        /// <summary>
        /// Clamps the values in the passed in vector
        /// by the positive and negative x and y of this
        /// vector.
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        public Vector2Int Clamp(Vector2Int values)
        {
            Vector2Int abs = Abs();
            return new Vector2Int(
                IntExtensions.Clamp(values.X, -1 * abs.X, abs.X),
                IntExtensions.Clamp(values.Y, -1 * abs.Y, abs.Y)
                );
        }

        /// <summary>
        /// Moves this vector towards 0 by the magnitude
        /// </summary>
        /// <param name="magnitude"></param>
        /// <returns></returns>
        public Vector2Int MoveTowardsZero(int magnitude)
        {
            return new Vector2Int(
                IntExtensions.MoveTowardsZero(X, magnitude),
                IntExtensions.MoveTowardsZero(Y, magnitude)
                );
        }

        public bool WithinRangeInclusive(Vector2Int range)
        {
            return X <= range.X && Y <= range.Y;
        }

        public bool WithinRangeInclusive(Vector2Int min, Vector2Int max)
        {
            return X >= min.X && X <= max.X && Y >= min.Y && Y <= max.Y;
        }

        public bool WithinRangeExclusive(Vector2Int range)
        {
            return X < range.X && Y < range.Y;
        }

        public bool WithinRangeExclusive(Vector2Int min, Vector2Int max)
        {
            return X > min.X && X < max.X && Y > min.Y && Y < max.Y;
        }

        public bool IsPositive()
        {
            return X >= 0 && Y >= 0;
        }

        public Vector2Int SetX(int x)
        {
            return new Vector2Int(x, Y);
        }

        public Vector2Int SetY(int y)
        {
            return new Vector2Int(X, y);
        }

        public int CompareTo(Vector2Int other)
        {
            int res = X.CompareTo(other.X);
            if (res == 0)
            {
                return Y.CompareTo(other.Y);
            }

            return res;
        }

        public override bool Equals(object obj)
        {
            if (obj is Vector2Int)
            {
                Vector2Int other = (Vector2Int)obj;
                if (X == other.X && Y == other.Y)
                {
                    return true;
                }
            }

            return false;
        }

        public override int GetHashCode()
        {
            return (new Tuple<int, int>(X, Y)).GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("({0}, {1})", X, Y);
        }

        #region Predefined Vectors
        /// <summary>
        /// (0, 0)
        /// </summary>
        public static Vector2Int Zero = new Vector2Int(0, 0);

        /// <summary>
        /// (1, 1)
        /// </summary>
        public static Vector2Int One = new Vector2Int(1, 1);

        /// <summary>
        /// (1, 0)
        /// </summary>
        public static Vector2Int Right = new Vector2Int(1, 0);

        /// <summary>
        /// (-1, 0)
        /// </summary>
        public static Vector2Int Left = new Vector2Int(-1, 0);

        /// <summary>
        /// (0, 1)
        /// </summary>
        public static Vector2Int Up = new Vector2Int(0, 1);

        /// <summary>
        /// (0, -1)
        /// </summary>
        public static Vector2Int Down = new Vector2Int(0, -1);
        #endregion

        public static int Distance(Vector2Int a, Vector2Int b)
        {
            return IntExtensions.ISqrt(((b.X - a.X) * (b.X - a.X)) + ((b.Y - a.Y) * (b.Y - a.Y)));
        }

        #region Enumerator Methods
        /// <summary>
        /// Returns all vector points starting from (0,0) and going to this vectors (x,y).
        /// Will increment towards negative or positive depending on values of x and y.
        /// </summary>
        /// <returns></returns>
        public IEnumerator<Vector2Int> GetEnumerator()
        {
            int xIncrement = X > 0 ? 1 : -1;
            int yIncrement = Y > 0 ? 1 : -1;
            for (int y = 0; y != Y; y = y + yIncrement)
            {
                for (int x = 0; x != X; x = x + xIncrement)
                {
                    yield return new Vector2Int(x, y);
                }
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        #endregion
    }
}
