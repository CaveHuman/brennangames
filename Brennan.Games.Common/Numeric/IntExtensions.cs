﻿using System;


namespace Brennan.Games.Common.Numeric
{
    public static class IntExtensions
    {
        public static int Clamp(int val, int min, int max)
        {
            if (val < min)
            {
                return min;
            }

            if (val > max)
            {
                return max;
            }

            return val;
        }

        public static int MoveTowardsZero(int val, int magnitude)
        {
            if (val < 0)
            {
                val = val + Math.Abs(magnitude);
                if (val > 0)
                {
                    val = 0;
                }
            }
            else if(val > 0)
            {
                val = val - Math.Abs(magnitude);
                if (val < 0)
                {
                    val = 0;
                }
            }

            return val;
        }

        public static int DivRoundUp(int val, int div)
        {
            return val % div == 0 ? (val / div) : (val / div) + 1;
        }

        /// <summary>
        /// From http://www.codecodex.com/wiki/Calculate_an_integer_square_root
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static int ISqrt(int num)
        {
            if (0 == num) { return 0; }  // Avoid zero divide  
            int n = (num / 2) + 1;       // Initial estimate, never low  
            int n1 = (n + (num / n)) / 2;
            while (n1 < n)
            {
                n = n1;
                n1 = (n + (num / n)) / 2;
            } // end while  
            return n;  
        }
    }
}
