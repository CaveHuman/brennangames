﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.Common.Numeric;

namespace Brennan.Games.Common.Collections
{
    public class Matrix <T> : IEnumerable<T>, ICloneable
    {

        private static int WIDTH_RANK = 0;
        private static int HEIGHT_RANK = 1;

        private T[,] matrix;

        public Vector2Int Length
        {
            get
            {
                return new Vector2Int(matrix.GetLength(WIDTH_RANK), matrix.GetLength(HEIGHT_RANK));
            }
        }

        public Matrix(Vector2Int size)
        {
            if (!size.IsPositive())
            {
                throw new ArgumentException("size must be positive (in both X and Y values)");
            }

            matrix = new T[size.X, size.Y];
        }

        public T this[Vector2Int index]
        {
            get
            {
                if (Contains(index))
                {
                    return matrix[index.X, index.Y];
                }
                else
                {
                    throw new IndexOutOfRangeException("index " + index + " must be positive and less than " + Length);
                }
            }

            set
            {
                if (Contains(index))
                {
                    matrix[index.X, index.Y] = value;
                }
                else
                {
                    throw new IndexOutOfRangeException("index " + index + " must be positive and less than " + Length);
                }
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            foreach(Vector2Int cell in Length)
            {
                yield return this[cell];
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public bool Contains(Vector2Int index)
        {
            return index != null && index.IsPositive() && index.WithinRangeExclusive(Length);
        }

        public object Clone()
        {
            Matrix<T> clone = new Matrix<T>(Length);
            foreach (Vector2Int point in Length)
            {
                clone[point] = this[point];
            }
            return clone;
        }
    }
}
