﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Brennan.Games.Common.Collections.Heaps
{
    public class PriorityQueue<T>
    {
        protected List<T> innerList;
        private Dictionary<T, int> indexMap;
        private IComparer<T> comparer;

        public PriorityQueue()
        {
            innerList = new List<T>();
            indexMap = new Dictionary<T, int>();
            if (typeof(IComparable).IsAssignableFrom(typeof(T)))
            {
                comparer = Comparer<T>.Default;
            }
            else
            {
                throw new ArgumentException("Value Typte T must implement IComparable, or an IComparer<T> must be supplied");
            }
        }

        public PriorityQueue(IComparer<T> comparer)
        {
            innerList = new List<T>();
            indexMap = new Dictionary<T, int>();
            this.comparer = comparer;
        }

        #region Public methods
        public List<T> getInnerList()
        {
            return innerList;
        }
        /// <summary>
        /// Pushes the entry onto the heap.
        /// </summary>
        /// <param name="entry"></param>
        public virtual void Push(T entry)
        {
            int childIndex = innerList.Count;
            //add at end of list
            innerList.Add(entry);
            //store the index for quick lookup
            setIndex(entry, childIndex);
            heapUp(childIndex);
        }

        public virtual T Pop()
        {
            //make sure there is something to return
            if (innerList.Count == 0) { return default(T); }
            
            //get the root, it should be the smallest
            T result = innerList[0];
            //set the root to the last node
            innerList[0] = innerList[innerList.Count - 1];
            //update the index for the new root
            setIndex(innerList[0], 0);
            //remove the last item (it is now in the root position)
            innerList.RemoveAt(innerList.Count - 1);
            //heap down the new root
            heapDown(0);
            return result;
        }

        public void Update(T entry)
        {
            int index = getIndex(entry);
            if (index >= 0)
            {
                Update(index);
            }
        }

        public T Peek()
        {
            if (innerList.Count > 0)
            {
                return innerList[0];
            }

            return default(T);
        }

        public bool Contains(T entry)
        {
            return getIndex(entry) != -1;
        }

        public void Clear()
        {
            innerList.Clear();
        }

        public int Count
        {
            get
            {
                return innerList.Count;
            }
        }
        #endregion

        #region Private Methods

        private void Update(int index)
        {
            //first heapUp the node
            int newIndex = heapUp(index);
            //next heapDown the node, only if the node was not moved up
            if (newIndex > index)
            {
                heapDown(newIndex);
            }
        }

        /// <summary>
        /// Moves the child up the heap until its parent is smaller than it is.
        /// </summary>
        /// <param name="childIndex"></param>
        /// <returns>Returns the childs new index</returns>
        private int heapUp(int childIndex)
        {
            int parentIndex;
            //loop until the root is the smallest
            while (true)
            {
                //if the index is 0, we are at the root
                if (childIndex == 0)
                {
                    break;
                }

                //look at the parent
                parentIndex = (childIndex - 1) / 2;

                //if child is less than parent, switch them
                if (Compare(childIndex, parentIndex) < 0)
                {
                    SwitchElements(childIndex, parentIndex);
                    childIndex = parentIndex;
                }
                else
                {
                    break;
                }
            }

            return childIndex;
        }

        /// <summary>
        /// Moves the parent node down the heap until its children are bigger than it is.
        /// </summary>
        /// <param name="parentIndex"></param>
        /// <returns>Returns the parents new index</returns>
        private int heapDown(int parentIndex)
        {
            int newParentIndex;
            int leftChildIndex;
            int rightChildIndex;

            //loop through so the root is the smallest again
            while(true) {
                leftChildIndex = 2 * parentIndex + 1;
                rightChildIndex = 2 * parentIndex + 2;

                //if the parent's left child exists, and the parent is bigger than the left child
                if (innerList.Count > leftChildIndex && Compare(parentIndex, leftChildIndex) > 0)
                {
                    //if the right child exists, and the left child is bigger than the right child
                    if (innerList.Count > rightChildIndex && Compare(leftChildIndex, rightChildIndex) > 0)
                    {
                        //the new parent will be the right
                        newParentIndex = rightChildIndex;
                    }
                    else
                    {
                        //the new parent will be the left
                        newParentIndex = leftChildIndex;
                    }
                }
                //if the parent's right child exists, and the parent is bigger than the right child
                else if (innerList.Count > rightChildIndex && Compare(parentIndex, rightChildIndex) > 0)
                {
                    //the new parent will be the right
                    newParentIndex = rightChildIndex;
                }
                else
                {
                    //if the parent is smaller than it's children, or we are at the end of the tree then we are done
                    break;
                }

                //switch the parent with the original
                SwitchElements(newParentIndex, parentIndex);

                parentIndex = newParentIndex;
            }

            return parentIndex;
        }

        /// <summary>
        /// Switches the elements at the two given indexes
        /// </summary>
        /// <param name="i"></param>
        /// <param name="j"></param>
        private void SwitchElements(int i, int j)
        {
            T h = innerList[i];
            innerList[i] = innerList[j];
            innerList[j] = h;

            //update index
            setIndex(innerList[i], i);
            setIndex(innerList[j], j);
        }
        #endregion

        #region Index Methods
        protected virtual void setIndex(T entry, int index)
        {
            indexMap[entry] = index;
        }

        protected virtual int getIndex(T entry)
        {
            int index;
            bool inMap = indexMap.TryGetValue(entry, out index);
            if (inMap)
            {
                return index;
            }
            else
            {
                return -1;
            }
        }

        protected virtual void clearIndex(T entry)
        {
            indexMap.Remove(entry);
        }

        protected virtual int Compare(int i, int j)
        {
            return comparer.Compare(innerList[i], innerList[j]);
        }
        #endregion
    }
}
