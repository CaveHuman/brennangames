﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Brennan.Games.Common.Collections
{
    public class ChainedEnumerable <T> : IEnumerable<T>
    {
        private List<IEnumerable<T>> enumerators;

        public ChainedEnumerable(params IEnumerable<T>[] enumerators)
        {
            this.enumerators = new List<IEnumerable<T>>();
            foreach (IEnumerable<T> enumerator in enumerators)
            {
                if (enumerator != null)
                {
                    this.enumerators.Add(enumerator);
                }
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            foreach (IEnumerable<T> enumerator in enumerators)
            {
                foreach(T val in enumerator)
                {
                    yield return val;
                }
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
