﻿using System;
using Brennan.Games.Common.Numeric;

namespace Brennan.Games.Common
{
    public class PhysicsObject : IHasBounds, IComparable<PhysicsObject>
    {
        /// <summary>
        /// Creates a Physics Object thats collider is determined by the colliderType enum.
        /// If the collider type is a Rectangle, the rectangles will be a square with length
        /// of two times radious.
        /// </summary>
        /// <param name="initialCenterGWN"></param>
        /// <param name="radiousGWN"></param>
        /// <param name="colliderType"></param>
        /// <param name="mass"></param>
        public PhysicsObject(Vector2Int initialCenterGWN, int radiousGWN, ColliderEnum colliderType, int mass)
        {
            Bounds = new Rectangle(initialCenterGWN, new Vector2Int(radiousGWN * 2, radiousGWN * 2));

            switch (colliderType)
            {
                case ColliderEnum.Circle:
                    Collider = new Circle(initialCenterGWN, radiousGWN);
                    break;
                case ColliderEnum.Rectangle:
                    Collider = new Rectangle(initialCenterGWN, new Vector2Int(radiousGWN * 2, radiousGWN * 2));
                    break;
            }

            ColliderType = colliderType;

            Radious = radiousGWN;
            Center = initialCenterGWN;

            //initialize with no velocity
            Velocity = new Vector2Int(0, 0);

            //Higher = more bouncy
            RestitutionGWN = 80; // = 0.2 * PhysicsEngine.GameWorldScale

            //mass only matters in relation to other objects, so it does not need to be scaled
            //should not be higher then the GameWorldScale however
            MassGWN = mass;
        }

        public PhysicsObject(Rectangle rect, int mass)
        {
            Bounds = rect;
            Collider = rect;

            Center = rect.Center;

            //initialize with no velocity
            Velocity = new Vector2Int(0, 0);

            //Higher = more bouncy
            RestitutionGWN = 80; // = 0.2 * PhysicsEngine.GameWorldScale

            //mass only matters in relation to other objects, so it does not need to be scaled
            //should not be higher then the GameWorldScale however
            MassGWN = mass;
        }

        public Vector2Int Velocity { get; set; }

        private Vector2Int center;
        public Vector2Int Center
        {
            get
            {
                return center;
            }
            set
            {
                center = value;
                Bounds.Center = value;
                Collider.Center = value;
            }
        }

        internal ColliderEnum ColliderType { get; set;  }

        internal ICollider Collider { get; set; }

        public int Radious { get; private set; }

        public Rectangle Bounds { get; private set; }

        /// <summary>
        /// Bouncy-ness during a collision
        /// </summary>
        internal int RestitutionGWN { get; set; }

        private int massGWN;
        internal int MassGWN
        {
            get
            {
                return massGWN;
            }
            set
            {
                massGWN = value;
                if (value == 0)
                {
                    inverseMassGWN = 0;
                }
                else
                {
                    inverseMassGWN = (1 * PhysicsEngine.GameWorldScale) / value; //this is 1/mass
                }
            }
        }

        private int inverseMassGWN;
        internal int InverseMassGWN
        {
            get { return inverseMassGWN; }
        }

        public int CompareTo(PhysicsObject other)
        {
            if (other == null) { return 1; }

            int res = Center.CompareTo(other.Center);

            if (res == 0)
            {
                return Radious.CompareTo(other.Radious);
            }

            return res;
        }

        object attached;

        public void Attach<T>(T t) where T:class
        {
            attached = t;
        }

        public T GetAttached<T>() where T:class
        {
            return attached as T;
        }
    }
}
