﻿using System;

namespace Brennan.Games.Common
{
    internal interface IHasBounds
    {
        Rectangle Bounds { get; }
    }
}
