﻿using System;


namespace Brennan.Games.Common
{
    /// <summary>
    /// Constants to get fractional numbers
    /// in terms of the Game World (after scaled).
    /// </summary>
    public struct GameWorldScale100
    {
        /// <summary>
        /// 0.1 in Game World Number
        /// 
        /// To get other tenths places multiply, ex:
        /// 0.2 would be OneTenths * 2
        /// </summary>
        public const int OneTenths = 10;

        /// <summary>
        /// 0.01 in Game World Number
        /// 
        /// To get other hundreths places multiple, ex:
        /// 0.02 would be OneHundreths * 2
        /// </summary>
        public const int OneHundreths = 1;
    }
}
