﻿using System;
using Brennan.Games.Common.Numeric;

namespace Brennan.Games.Common
{
    public class Circle : ICollider
    {
        public Vector2Int Center { get; set; }
        public int Radious { get; set; }

        public Circle(Vector2Int center, int radious)
        {
            Center = center;
            Radious = radious;
        }

        public ICollider ScaleUp(int scale)
        {
            return new Circle(Center * scale, Radious * scale);
        }

        public ICollider ScaleDown(int scale)
        {
            return new Circle(Center / scale, Radious / scale);
        }

        #region Circle Int Operators
        public static Circle operator *(Circle c, int a)
        {
            return new Circle(c.Center * a, c.Radious * a);
        }

        public static Circle operator *(int a, Circle c)
        {
            return c * a;
        }

        public static Circle operator /(Circle c, int a)
        {
            return new Circle(c.Center / a, c.Radious / a);
        }

        public static Circle operator /(int a, Circle c)
        {
            return new Circle(a / c.Center, a / c.Radious);
        }
        #endregion Circle Int Operators
    }
}
