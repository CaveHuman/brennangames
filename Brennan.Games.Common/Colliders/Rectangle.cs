﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.Common.Numeric;

namespace Brennan.Games.Common
{
    public class Rectangle : ICollider
    {
        private Vector2Int size;
        private Vector2Int center;

        public Vector2Int Center
        {
            get
            {
                return center;
            }
            set
            {
                center = value;
                Update();
            }
        }

        public Vector2Int Size
        {
            get
            {
                return size;
            }
            set
            {
                size = value;
                Update();
            }
        }

        private Vector2Int extentsMin, extentsMax, max, min;

        public Vector2Int ExtentsMin
        {
            get { return extentsMin; }
        }

        public Vector2Int ExtentsMax
        {
            get { return extentsMax; }
        }

        public Vector2Int Max
        {
            get { return max; }
        }

        public Vector2Int Min
        {
            get { return min; }
        }

        public Rectangle(Vector2Int center, Vector2Int size)
        {
            this.center = center;
            this.size = size;
            Update();
        }

        /// <summary>
        /// Creates a new rectangle with size width and height.
        /// The center will be set to size / 2.
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public Rectangle(int width, int height)
        {
            this.size = new Vector2Int(width, height);
            this.center = this.size / 2;
            Update();
        }

        public Rectangle(Vector2Int size)
        {
            this.size = size;
            this.center = this.size / 2;
            Update();
        }

        /// <summary>
        /// Checks if this rectangle contains fully the passed in rectangle.
        /// </summary>
        /// <param name="r"></param>
        /// <returns></returns>
        public bool Contains(Rectangle r)
        {
            return r.min.X >= min.X &&
                   r.max.X <= max.X &&
                   r.min.Y >= min.Y &&
                   r.max.Y <= max.Y;
        }

        /// <summary>
        /// Checks if this rectangle intersects with the passed in rectangle.
        /// </summary>
        /// <param name="r"></param>
        /// <returns></returns>
        public bool Intersect(Rectangle r)
        {
            if (max.X <= r.min.X) return false; //we are to the left of r
            if (min.X >= r.max.X) return false; //we are to the right of r
            if (max.Y <= r.min.Y) return false; //we are above r
            if (min.Y >= r.max.Y) return false; //we are below r
            return true;
        }

        private void Update()
        {
            extentsMin = size / 2;
            extentsMax = size.DivRoundUp(2);
            max = center + extentsMax;
            min = center - extentsMin;
        }

        public ICollider ScaleUp(int scale)
        {
            return new Rectangle(center * scale, size * scale);
        }

        public ICollider ScaleDown(int scale)
        {
            return new Rectangle(center / scale, size / scale);
        }

        #region Rectangle Int Operators
        public static Rectangle operator *(Rectangle r, int i)
        {
            return new Rectangle(r.Center * i, r.Size * i);
        }

        public static Rectangle operator *(int i, Rectangle r)
        {
            return r * i;
        }

        public static Rectangle operator /(Rectangle r, int i)
        {
            return new Rectangle(r.Center / i, r.Size / i);
        }

        public static Rectangle operator /(int i, Rectangle r)
        {
            return new Rectangle(i / r.Center, r.Size / i);
        }

        #endregion Rectangle Int Operators

        #region Builder Methods

        /// <summary>
        /// Hidden no argument constructor to enable builder methods
        /// </summary>
        private Rectangle() { }

        /// <summary>
        /// Builds a new Rectangle from the given points. Used as the regular Rectangle constructor
        /// takes the same arguments but interpreted as Center and Size.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Rectangle FromPoints(Vector2Int a, Vector2Int b)
        {
            Rectangle r = new Rectangle();
            r.max = new Vector2Int(Math.Max(a.X, b.X), Math.Max(a.Y, b.Y));
            r.min = new Vector2Int(Math.Min(a.X, b.X), Math.Min(a.Y, b.Y));
            r.size = r.max - r.min;
            r.extentsMin = r.size / 2;
            r.extentsMax = r.size.DivRoundUp(2);
            r.center = r.min + r.extentsMin;
            return r;
        }
        #endregion
    }
}
