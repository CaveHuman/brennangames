﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Brennan.Games.Lockstep
{
    public interface IHasGameFrame
    {
        public void GameFrameTurn(int gameFramesPerSecond);
    }
}
