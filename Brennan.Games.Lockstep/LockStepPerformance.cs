﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.Lockstep.Actions;

namespace Brennan.Games.Lockstep
{
    public class LockStepPerformance
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static int INITIAL_NETWORK_AVERAGE = 200; //in Milliseconds
        private static readonly int MAX_NETWORK_AVERAGE = 300;
        private static readonly int MAX_RUNTIME_AVERAGE = 300;
        public static int INITIAL_GAME_FRAME_TURN_LENGTH = 50; //in Milliseconds

        private RollingAverage networkAverage;
        private RollingAverage runtimeAverage;

        public LockStepPerformance(int numberOfPlayers)
        {
            networkAverage = new RollingAverage(numberOfPlayers, INITIAL_NETWORK_AVERAGE, MAX_NETWORK_AVERAGE);
            runtimeAverage = new RollingAverage(numberOfPlayers, INITIAL_GAME_FRAME_TURN_LENGTH, MAX_RUNTIME_AVERAGE);
        }

        public void RecordPerformance(Action[] actions)
        {
            for (int playerID = 0; playerID < actions.Length; playerID++)
            {
                networkAverage.Add(actions[playerID].NetworkAverage, playerID);
                runtimeAverage.Add(actions[playerID].RuntimeAverage, playerID);
            }
        }

        public int getMaxNetworkAverage()
        {
            return networkAverage.GetMax();
        }

        public int getMaxRuntimeAverage()
        {
            return runtimeAverage.GetMax();
        }

        public void logGameFrameRate(LockStepTurn lockStepTurn)
        {
            /***Log performance***/
            //log.Debug ("GameFramesPerSecond is " + GameFramesPerSecond);
            StringBuilder performanceLog = new StringBuilder();
            //add each players current network value
            foreach (int val in networkAverage.currentValues)
            {
                performanceLog.Append(val + ", ");
            }
            //add the network average
            performanceLog.Append(networkAverage.GetMax() + ", ");
            //add each players current runtime value
            foreach (int val in runtimeAverage.currentValues)
            {
                performanceLog.Append(val + ", ");
            }
            //add the runtime average
            performanceLog.Append(runtimeAverage.GetMax() + ", ");

            //add other metrics
            performanceLog.Append(lockStepTurn.GameFramesPerSecond + ", ");
            performanceLog.Append(lockStepTurn.LockStepsPerSecond + ", ");
            performanceLog.Append(lockStepTurn.GameFramesPerLockstepTurn);

            log.Info(performanceLog.ToString());
        }

        private static void logGameFrameRateHeaders(int numberOfPlayers)
        {
            StringBuilder headers = new StringBuilder();
            for (int i = 0; i < numberOfPlayers; i++)
            {
                headers.Append("P" + i + " NetworkAverage, ");
            }
            headers.Append("MaxNetworkAverage, ");
            for (int i = 0; i < numberOfPlayers; i++)
            {
                headers.Append("P" + i + " RuntimeAverage, ");
            }
            headers.Append("MaxRuntimeAverage, ");
            headers.Append("GameFramesPerSecond, ");
            headers.Append("LockstepsPerSecond, ");
            headers.Append("GameFramesPerLockstepTurn");

            log.Info(headers.ToString());
        }
    }
}
