﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Brennan.Games.Lockstep
{
    public class LockStepTurn
    {
        public static readonly int FirstLockStepTurnID = 0;
        private static long INITIAL_GAME_FRAME_TURN_LENGTH = 50; //In Milliseconds

        public int LockStepTurnID { get; private set; }
        public int GameFramesPerLockstepTurn { get; private set; }
        public int GameFrameTurnLength { get; private set; }
        public int GameFramesPerSecond { get; private set; }
        public int LockStepsPerSecond { get; private set; }
        public long MaxGameFrameRuntime { get; internal set; }
        public bool Finished
        {
            get
            {
                return currentGameFrame == GameFramesPerLockstepTurn;
            }
        }
        public bool IsFirstTurn { get { return LockStepTurnID == FirstLockStepTurnID; } }
        public bool IsSecondTurn { get { return LockStepTurnID == FirstLockStepTurnID + 1; } }
        public bool IsAfterSecondTurn { get { return LockStepTurnID > FirstLockStepTurnID + 1; } }

        private int currentGameFrame = 0;
        private Stopwatch gameTurnSW = new Stopwatch();

        public LockStepTurn()
        {
            LockStepTurnID = FirstLockStepTurnID;
            MaxGameFrameRuntime = INITIAL_GAME_FRAME_TURN_LENGTH;
        }

        public LockStepTurn(int lockStepTurnID, int gameFramesPerLockstepTurn, int gameFrameTurnLength, int gameFramesPerSecond, int lockStepsPerSecond)
        {
            LockStepTurnID = lockStepTurnID;
            GameFramesPerLockstepTurn = gameFramesPerLockstepTurn;
            GameFrameTurnLength = gameFrameTurnLength;
            GameFramesPerSecond = gameFramesPerSecond;
            LockStepsPerSecond = lockStepsPerSecond;
            MaxGameFrameRuntime = 0;
        }

        public bool After(LockStepTurn lockStepTurnGameFrameRate)
        {
            return LockStepTurnID > lockStepTurnGameFrameRate.LockStepTurnID;
        }

        public int Tick(float deltaTime, int accumilatedTime, LockStepGameEngine.OnGameFrame onGameFrame)
        {
            //Basically same logic as FixedUpdate, but we can scale it by adjusting FrameLength
            accumilatedTime = accumilatedTime + Convert.ToInt32((deltaTime * 1000)); //convert sec to milliseconds

            //in case the FPS is too slow, we may need to update the game multiple times a frame
            while (accumilatedTime > GameFrameTurnLength)
            {
                GameFrameTurn(deltaTime, onGameFrame);
                accumilatedTime = accumilatedTime - GameFrameTurnLength;
            }

            return accumilatedTime;
        }

        private void GameFrameTurn(float deltaTime, LockStepGameEngine.OnGameFrame onGameFrame)
        {
            //start the stop watch to determine game frame runtime performance
            gameTurnSW.Start();

            //update game
            if (onGameFrame != null)
            {
                onGameFrame.Invoke(GameFramesPerSecond);
            }

            currentGameFrame++;

            //stop the stop watch, the gameframe turn is over
            gameTurnSW.Stop();
            //update only if it's larger - we will use the game frame that took the longest in this lockstep turn
            long runtime = Convert.ToInt32((deltaTime * 1000))/*deltaTime is in secounds, convert to milliseconds*/ + gameTurnSW.ElapsedMilliseconds;
            if (runtime > MaxGameFrameRuntime)
            {
                MaxGameFrameRuntime = runtime;
            }
            //clear for the next frame
            gameTurnSW.Reset();
        }
    }
}
