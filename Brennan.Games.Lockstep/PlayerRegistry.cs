﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.Common.Collections;

namespace Brennan.Games.Lockstep
{
    public class PlayerRegistry
    {
        private int expectedNumberOfPlayers;
        private IDictionary<string, NetworkPlayer> playerIDsToNetworkPlayers;

        public PlayerRegistry(int numberOfPlayers)
        {
            expectedNumberOfPlayers = numberOfPlayers;
            playerIDsToNetworkPlayers = new OrderedDictionary<string, NetworkPlayer>(numberOfPlayers);
        }

        public void registerPlayer(NetworkPlayer player)
        {
            playerIDsToNetworkPlayers.Add(player.ToString(), player);
        }

        public bool isPlayerIDRegistered(string playerID)
        {
            return playerIDsToNetworkPlayers.ContainsKey(playerID);
        }

        public NetworkPlayer player(string playerID)
        {
            return playerIDsToNetworkPlayers[playerID];
        }

        public bool allPlayersRegistered()
        {
            return playerIDsToNetworkPlayers.Count >= expectedNumberOfPlayers;
        }

        public int registeredPlayerCount()
        {
            return playerIDsToNetworkPlayers.Count;
        }
    }
}
