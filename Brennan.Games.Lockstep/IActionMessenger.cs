﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.Lockstep.Actions;

namespace Brennan.Games.Lockstep
{
    interface IActionMessenger
    {
        public void SendActionToAllOtherPlayers(int currentLockStepTurnID, int sendingPlayerID, Action action);
    }
}
