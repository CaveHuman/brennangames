﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Brennan.Games.Lockstep.Actions
{
    [Serializable]
    public class NoAction : Action
    {
        public override void ProcessAction() { }
    }
}
