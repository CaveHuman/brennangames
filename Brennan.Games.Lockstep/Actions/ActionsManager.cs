﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Brennan.Games.Lockstep.Serialization;

namespace Brennan.Games.Lockstep.Actions
{
    public class ActionsManager
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        

        private IActionMessenger actionMessenger;
        private Queue<Action> actionsToSend;
        private PendingActions pendingActions;
        private ConfirmedActions confirmedActions;
        private ActionProcesser actionProcessor;
        private int initialNetworkAverage;
        private int playerID;

        private LockStepTurn currentLockStepTurn;
        private long processActionsRunTime;

        public ActionsManager(IActionMessenger actionMessenger, int numberOfPlayers, int playerID, int initialNetworkAverage)
        {
            this.actionMessenger = actionMessenger;
            this.actionProcessor = new ActionProcesser();
            this.initialNetworkAverage = initialNetworkAverage;
            this.playerID = playerID;
            actionsToSend = new Queue<Action>();
            pendingActions = new PendingActions(numberOfPlayers, LockStepTurn.FirstLockStepTurnID);
            confirmedActions = new ConfirmedActions(numberOfPlayers, LockStepTurn.FirstLockStepTurnID);
            log.Debug("Number of players is " + numberOfPlayers);
        }

        public void SetCurrentLockStepTurn(LockStepTurn lockStepTurn)
        {
            currentLockStepTurn = lockStepTurn;
        }

        public void EnqueueActionToSend(Action action)
        {
            actionsToSend.Enqueue(action);
        }

        public void RecievePlayersAction(Action action, int playerID, int actionsLockStepTurn)
        {
            pendingActions.AddAction(action, playerID, currentLockStepTurn.LockStepTurnID, actionsLockStepTurn);
        }

        public void ConfirmAction(int confirmingPlayerID, int confirmedActionLockStepTurn)
        {
            confirmedActions.ConfirmAction(confirmingPlayerID, currentLockStepTurn.LockStepTurnID, confirmedActionLockStepTurn);
        }

        public Action[] ProcessActions()
        {
            //move the confirmed actions to next turn
            confirmedActions.NextTurn();
            //move the pending actions to this turn
            pendingActions.NextTurn();
            sendPendingAction(currentLockStepTurn.MaxGameFrameRuntime + processActionsRunTime);
            if (currentLockStepTurn.IsAfterSecondTurn)
            {
                processActionsRunTime = actionProcessor.ProcessActions(pendingActions.CurrentActions);
                return pendingActions.CurrentActions;
            }
            return null;
        }

        public bool ReadyForNextTurn()
        {
            if (confirmedActions.ReadyForNextTurn(currentLockStepTurn.LockStepTurnID))
            {
                if (pendingActions.ReadyForNextTurn(currentLockStepTurn.LockStepTurnID))
                {
                    return true;
                }
                else
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("Have not recieved player(s) actions: ");
                    foreach (int i in pendingActions.WhosNotReady(currentLockStepTurn.LockStepTurnID))
                    {
                        sb.Append(i + ", ");
                    }
                    log.Debug(sb.ToString());
                }
            }
            else
            {
                log.Debug("ConfirmedActions not ready for next turn. ");
                int[] whosNotReady = confirmedActions.WhosNotConfirmed(currentLockStepTurn.LockStepTurnID);
                if (whosNotReady != null)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("Have not recieved confirmation from player(s): ");
                    foreach (int i in whosNotReady)
                    {
                        sb.Append(i + ", ");
                    }
                    log.Debug(sb.ToString());
                }
            }
            return false;
        }

        private void sendPendingAction(long gameFrameRuntime)
        {
            Action action = determineNextAction();
            if (currentLockStepTurn.IsAfterSecondTurn)
            {
                action.NetworkAverage = confirmedActions.GetPriorTime();
            }
            else
            {
                action.NetworkAverage = initialNetworkAverage;
            }
            action.RuntimeAverage = Convert.ToInt32(gameFrameRuntime);

            //add action to our own list of actions to process
            RecievePlayersAction(action, playerID, currentLockStepTurn.LockStepTurnID);
            //start the confirmed action timer for network average
            confirmedActions.StartTimer();
            //confirm our own action
            ConfirmAction(playerID, currentLockStepTurn.LockStepTurnID);
            //send action to all other players
            actionMessenger.SendActionToAllOtherPlayers(currentLockStepTurn.LockStepTurnID, playerID, action);
            log.Debug("Sent " + (action.GetType().Name) + " action for turn " + currentLockStepTurn.LockStepTurnID);
        }

        private Action determineNextAction()
        {
            Action action = null;
            if (actionsToSend.Count > 0)
            {
                action = actionsToSend.Dequeue();
            }

            //if no action for this turn, send the NoAction action
            if (action == null)
            {
                action = new NoAction();
            }
            return action;
        }
    }
}
