﻿using System;

namespace Brennan.Games.Lockstep.Actions
{
    [Serializable]
    public abstract class Action
    {
        public int NetworkAverage { get; set; }
        public int RuntimeAverage { get; set; }

        public virtual void ProcessAction() { }
    }
}
