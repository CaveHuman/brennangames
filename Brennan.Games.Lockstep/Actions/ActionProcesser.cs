﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Brennan.Games.Lockstep.Actions
{
    public class ActionProcesser
    {
        private Stopwatch gameTurnSW;
        private int playerIDToProcessFirst = 0; //used to rotate what player's action gets processed first

        public ActionProcesser()
        {
            gameTurnSW = new Stopwatch();
        }

        public long ProcessActions(Action[] currentActions)
        {
            //process action should be considered in runtime performance
            gameTurnSW.Reset();
            gameTurnSW.Start();

            //Rotate the order the player actions are processed so there is no advantage given to
            //any one player
            for (int playerID = playerIDToProcessFirst; playerID < currentActions.Length; playerID++)
            {
                currentActions[playerID].ProcessAction();
            }

            for (int playerID = 0; playerID < playerIDToProcessFirst; playerID++)
            {
                currentActions[playerID].ProcessAction();
            }

            playerIDToProcessFirst++;
            if (playerIDToProcessFirst >= currentActions.Length)
            {
                playerIDToProcessFirst = 0;
            }

            //finished processing actions for this turn, stop the stopwatch
            gameTurnSW.Stop();
            return gameTurnSW.ElapsedMilliseconds;
        }
    }
}
