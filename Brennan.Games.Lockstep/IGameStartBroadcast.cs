﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Brennan.Games.Lockstep
{
    public interface IGameStartBroadcast
    {
        public delegate void MatchMakerPrepGameEvent(int numberOfPlayers);
        public delegate void MatchMakerOnGameStartEvent();
        public delegate void MatchMakerOnGameStartServerEvent(PlayerRegistry playerRegistry);

        public void NotifyOnGameStart(MatchMakerOnGameStartEvent callBack);
        public void NotifyOnGameStartServer(MatchMakerOnGameStartServerEvent callBack);
        public void NotifyOnPrepGame(MatchMakerPrepGameEvent callBack);
    }
}
