﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.Lockstep.Actions;

namespace Brennan.Games.Lockstep
{
    public class LockStepMainGameLoop
    {
        public delegate void OnLockStepTurn(LockStepTurn currentLockStepTurn);

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private ActionsManager actionsManager;
        private LockStepTurnFactory lockStepTurnFactory;
        private LockStepTurn currentLockStepTurn;
        private OnLockStepTurn onLockStepTurn;
        private int accumilatedTime = 0; //the accumilated time in Milliseconds that have passed since the last time GameFrame was called

        public LockStepMainGameLoop(ActionsManager actionsManager, LockStepTurnFactory lockStepTurnFactory)
        {
            this.actionsManager = actionsManager;
            this.lockStepTurnFactory = lockStepTurnFactory;
            currentLockStepTurn = lockStepTurnFactory.ZeroTurn();
            actionsManager.SetCurrentLockStepTurn(currentLockStepTurn);
            NotifyOnLockStepTurn(actionsManager.SetCurrentLockStepTurn);
        }

        public void NotifyOnLockStepTurn(OnLockStepTurn callBack)
        {
            onLockStepTurn += callBack;
        }

        public void Tick(float deltaTime, LockStepGameEngine.OnGameFrame onGameFrame)
        {
            log.Debug("LockStepTurnID is " + currentLockStepTurn.LockStepTurnID);
            if (currentLockStepTurn.Finished)
            {
                if (actionsManager.ReadyForNextTurn())
                {
                    Action[] processedActions = actionsManager.ProcessActions();
                    if (processedActions != null)
                    {
                        lockStepTurnFactory.RecordActionsPerformance(processedActions);
                    }
                    currentLockStepTurn = lockStepTurnFactory.CalculateNextLockStepTurn(currentLockStepTurn.LockStepTurnID + 1);
                    if (onLockStepTurn != null)
                    {
                        onLockStepTurn.Invoke(currentLockStepTurn);
                    }
                    accumilatedTime = currentLockStepTurn.Tick(deltaTime, accumilatedTime, onGameFrame);
                }
            }
            else
            {
                accumilatedTime = currentLockStepTurn.Tick(deltaTime, accumilatedTime, onGameFrame);
            }
        }
    }
}
