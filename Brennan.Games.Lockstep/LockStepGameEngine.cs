﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Brennan.Games.Lockstep.Actions;
using Brennan.Games.Lockstep.Serialization;

namespace Brennan.Games.Lockstep
{
    public class LockStepGameEngine : MonoBehaviour, IActionMessenger
    {

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Public Variables
        public static LockStepGameEngine Instance;

        public IGameStartBroadcast gameStartBroadcast;
        public int numberOfPlayers;

        public delegate void OnGameFrame(int gameFramesPerSecond);
        #endregion

        #region Private Variables
        private NetworkView nv;
        private PlayerRegistry playerRegistry;

        private bool gameStarted = false; //indicates if the game has started

        private ActionsManager actionsManager;

        private int gameFrame = 0; //Current Game Frame number in the currect lockstep turn
        private int accumilatedTime = 0; //the accumilated time in Milliseconds that have passed since the last time GameFrame was called
        private bool initialized = false;

        private OnGameFrame onGameFrame;
        #endregion

        #region Public API
        public void setGameStartBroadcast(IGameStartBroadcast gameStartBroadcast)
        {
            this.gameStartBroadcast = gameStartBroadcast;
        }

        public void notifyOnGameFrame(OnGameFrame callBack)
        {
            this.onGameFrame += callBack;
        }

        public void AddAction(Action action)
        {
            log.Debug("Action Added");
            if (!gameStarted)
            {
                log.Debug("Game has not started, action will be ignored.");
                return;
            }
            actionsManager.EnqueueActionToSend(action);
        }
        #endregion

        #region Unity called methods
        // Use this for initialization - Called by Unity on startup
        void Start()
        {
            enabled = false;

            Instance = this;
            nv = GetComponent<NetworkView>();

            gameStartBroadcast.NotifyOnPrepGame(PrepGameStart);
            gameStartBroadcast.NotifyOnGameStartServer(GameStartServer);
            gameStartBroadcast.NotifyOnGameStart(GameStart);
        }
        
        //called once per unity frame by Unity
        public void Update()
        {
            actionsManager.Tick(Time.deltaTime, onGameFrame);
        }
        #endregion

        #region GameStart
        private void PrepGameStart(int numberOfPlayers)
        {
            log.Debug("Prep Game called. My PlayerID: " + Network.player.ToString());
            this.numberOfPlayers = numberOfPlayers;
            actionsManager = new ActionsManager(this, numberOfPlayers, Convert.ToInt32(Network.player.ToString()), LockStepTurnFactory.INITIAL_NETWORK_AVERAGE);
            initialized = true;
        }

        private void GameStartServer(PlayerRegistry playerRegistry)
        {
            this.playerRegistry = playerRegistry;
        }

        private void GameStart()
        {
            //start the LockStep Turn loop
            enabled = true;
            gameStarted = true;
        }
        #endregion

        #region Actions
        public void SendActionToAllOtherPlayers(int currentLockStepTurnID, int sendingPlayerID, Action action)
        {
            nv.RPC("RecieveAction", RPCMode.Others, currentLockStepTurnID, sendingPlayerID.ToString(), BinarySerialization.SerializeObjectToByteArray(action));
        }

        [RPC]
        public void RecieveAction(int actionsLockStepTurn, string playerID, byte[] actionAsBytes)
        {
            log.Debug("Recieved Player " + playerID + "'s action for turn " + actionsLockStepTurn + " on our turn" + actionsManager.CurrentLockStepTurnID());
            Action action = BinarySerialization.DeserializeObject<Action>(actionAsBytes);
            if (action == null)
            {
                log.Debug("Recieved null action from player " + playerID + " for player's turn " + actionsLockStepTurn + "on our turn " + actionsManager.CurrentLockStepTurnID());
                //TODO: Error handle invalid actions recieve
            }
            else
            {
                actionsManager.RecievePlayersAction(action, Convert.ToInt32(playerID), actionsLockStepTurn);

                //send confirmation
                if (Network.isServer)
                {
                    //we don't need an rpc call if we are the server
                    ConfirmActionServer(actionsLockStepTurn, Network.player.ToString(), playerID);
                }
                else
                {
                    nv.RPC("ConfirmActionServer", RPCMode.Server, actionsLockStepTurn, Network.player.ToString(), playerID);
                }
            }
        }

        [RPC]
        public void ConfirmActionServer(int lockStepTurn, string confirmingPlayerID, string confirmedPlayerID)
        {
            if (!Network.isServer) { return; } //Workaround - if server and client on same machine

            log.Debug("ConfirmActionServer called for turn" + lockStepTurn + " and playerID " + confirmingPlayerID);
            log.Debug("Sending Confirmation to player " + confirmedPlayerID);

            if (Network.player.ToString().Equals(confirmedPlayerID))
            {
                //we don't need an RPC call if this is the server
                ConfirmAction(lockStepTurn, confirmingPlayerID);
            }
            else
            {
                nv.RPC("ConfirmAction", playerRegistry.player(confirmedPlayerID), lockStepTurn, confirmingPlayerID);
            }
        }

        [RPC]
        public void ConfirmAction(int confirmedActionLockStepTurn, string confirmingPlayerID)
        {
            actionsManager.ConfirmAction(Convert.ToInt32(confirmingPlayerID), confirmedActionLockStepTurn);
        }
        #endregion
    }
}
