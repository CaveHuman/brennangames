﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.Lockstep.Actions;

namespace Brennan.Games.Lockstep
{
    public class LockStepTurnFactory
    {
        
        private LockStepPerformance lockStepPerformance;

        public LockStepTurnFactory(int numberOfPlayers)
        {
            this.lockStepPerformance = new LockStepPerformance(numberOfPlayers);
        }

        public LockStepTurn ZeroTurn()
        {
            LockStepTurn lockStepTurn = new LockStepTurn();
            return lockStepTurn;
        }

        public LockStepTurn CalculateNextLockStepTurn(int lockStepTurnID)
        {
            //log.Debug ("Runtime Average is " + runtimeAverage.GetMax ());
            //log.Debug ("Network Average is " + networkAverage.GetMax ());
            int lockstepTurnLength = (lockStepPerformance.getMaxNetworkAverage() * 2/*two round trips*/) + 1/*minimum of 1 ms*/;
            int gameFrameTurnLength = lockStepPerformance.getMaxRuntimeAverage();

            //lockstep turn has to be at least as long as one game frame
            if (gameFrameTurnLength > lockstepTurnLength)
            {
                lockstepTurnLength = gameFrameTurnLength;
            }

            int gameFramesPerLockstepTurn = lockstepTurnLength / gameFrameTurnLength;
            //if gameframe turn length does not evenly divide the lockstep turn, there is extra time left after the last
            //game frame. Add one to the game frame turn length so it will consume it and recalculate the Lockstep turn length
            if (lockstepTurnLength % gameFrameTurnLength > 0)
            {
                gameFrameTurnLength++;
                lockstepTurnLength = gameFramesPerLockstepTurn * gameFrameTurnLength;
            }

            int lockstepsPerSecond = (1000 / lockstepTurnLength);
            if (lockstepsPerSecond == 0) { lockstepsPerSecond = 1; } //minimum per second

            int gameFramesPerSecond = lockstepsPerSecond * gameFramesPerLockstepTurn;

            LockStepTurn nextLockStepTurn = new LockStepTurn(lockStepTurnID, gameFramesPerLockstepTurn, gameFrameTurnLength, gameFramesPerSecond, lockstepsPerSecond);
            lockStepPerformance.logGameFrameRate(nextLockStepTurn);
            return nextLockStepTurn;
        }

        public void RecordActionsPerformance(Action[] actions)
        {
            lockStepPerformance.RecordPerformance(actions);
        }
    }
}
